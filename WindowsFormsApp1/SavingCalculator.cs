﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{

    class SavingCalculator
    {
        private double monthlyDeposit;
        private double interestRate;
        private double period;
        private double balance;

        public SavingCalculator()
        {
            this.interestRate = 0.1/12;
      
        }
        public void CalculateInterest()
        {
            int monthlyPeriod = Convert.ToInt32(GetPeriod());
            double interestEarned = 0.0;
            balance = 0;
            for (int i = 1; i <= monthlyPeriod; i++)
            {
                interestEarned = interestRate * balance;
                balance += interestEarned + monthlyDeposit;
                
            }

        }

        public double GetMonthlyDep()
        {
            return monthlyDeposit;
        }
        public void SetMonthlyDep(double monthlyDeposit)
        {
            if (monthlyDeposit >= 0.00)
            {
                this.monthlyDeposit = monthlyDeposit;
            }
        }
        
        public double GetPeriod()
        {
            return period * 12;
        }

        public void SetPeriod(double period)
        {
            if (period > 0.00)
            {
                this.period = period;
            }
        }
        public double GetBalance()
        {
            return balance;
        }
        public void SetBalance(double balance)
        {
            if (balance > 0.00)
            {
                this.balance = balance;
            }

        }



    }



    
  
}
