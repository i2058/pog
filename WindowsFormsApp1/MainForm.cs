﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class MainForm : Form
    {
       
        private string name = string.Empty;
        private BMICalculator bmiCalc = new BMICalculator();
        private SavingCalculator savingCalc = new SavingCalculator();
        public MainForm()
        {
            InitializeComponent();

            InitializeGUI();

        }
        private void InitializeGUI()
        {
            this.Text += " Simon Sandqvist";
            lblBMI.Text = string.Empty;
            lblWeightCat.Text = string.Empty;
            lblNormalBMI.Text = "Normal weight";
            rbtnMetric.Checked = true;
            lblNormalBMI.Text = "Normal BMI is between 18.5 and 24.9";

        }
        private void UpdateHeightText()
        {
            if (rbtnMetric.Checked)
            {
                lblHeight.Text = "Height (cm)";
                lblWeight.Text = "Weight (kg)";
                txtInch.Visible = false;
            }
            else
            {
                lblHeight.Text = "Height (ft, in)";
                lblWeight.Text = "Weight (lbs)";
                txtInch.Visible = true;
            }
            lblWeightCat.Text = "";
            lblBMI.Text = "";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

     

        private void button1_Click(object sender, EventArgs e)
        {
           bool ok = ReadInputBMI();

            if (ok)
            {
                CalculateAndDisplayResults();
            }

        }


        private void CalculateAndDisplayResults()
        {
            double bmi = bmiCalc.CalculateBMI();
            lblBMI.Text = bmi.ToString("0.00");
            lblWeightCat.Text = bmiCalc.BMIWeightCategory();
        }
        private bool ReadInputBMI()
        {
            ReadName();
            bool heightOK = ReadHeight();
            bool weightOK = ReadWeight();
            ReadUnit();

            return heightOK && weightOK;
        }
        private void ReadUnit()
        {
            if (rbtnMetric.Checked)
                bmiCalc.SetUnitType(UnitTypes.Metric);

            else 
                bmiCalc.SetUnitType(UnitTypes.Imperial);

        }


        private void ReadName()
        {
            name = txtName.Text;
            name = name.Trim();
            grpResults.Text = name;
        }

        private bool ReadHeight()
        {
            double height = 0.0;

            bool ok = double.TryParse(txtCmFt.Text, out height);
            if (!ok)
            {
                MessageBox.Show("The height value is invalid!", "Error");
            }

            double inch = 0.0;
            if (rbtnImperial.Checked)
            {
                ok = ok && double.TryParse(txtInch.Text, out inch);
                if (!ok)
                {
                    MessageBox.Show("The inch value is invalid!", "Error");
                }
                else
                {
                    height = height * 12.0 + inch; //Conversion from feet to inches
                }
            } 
            else
            {
                height = height / 100.0; //Conversion from cm to m
            }

            bmiCalc.SetHeight(height);
            return ok;
        }
        
        private bool ReadWeight()
        { 
            double weight = 0.0;
            bool ok = double.TryParse(txtWeight.Text, out weight);
            if (!ok)
            {
                MessageBox.Show("The weight value is invalid!", "Error");
            }
            bmiCalc.SetWeight(weight);

            return ok;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            UpdateHeightText();
        }
        private void rbtnMetric_CheckedChanged(object sender, EventArgs e)
        {
            UpdateHeightText();
        }

        private void lblBMI_Click(object sender, EventArgs e)
        {
        
        }

        private void CalcDisplayAmPaid()
        {
            double amPaid = savingCalc.GetMonthlyDep() * savingCalc.GetPeriod();
            lblAmPaid.Text = amPaid.ToString("f2");

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            CalcDisplayAmPaid();
            savingCalc.CalculateInterest();
            lblBal.Text = savingCalc.GetBalance().ToString("f2");

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        private double ReadDouble(string str, out bool success)
        {
            double value = -1.00;
            success = false;
            if (double.TryParse(str, out value))
                success = true;

            return value;
        }


        private void ReadSavingInput()
        {
            bool success = true;

            double monthlyDeposit = ReadDouble(txtDeposit.Text, out success);
            if (success)
                savingCalc.SetMonthlyDep(monthlyDeposit);
            else
            {
                MessageBox.Show("Invalid value for monthly deposit!");
                return;
            }
        }

        private void ReadPeriodInput()
        {
            bool success = true;

            double period = ReadDouble(txtPeriod.Text, out success);
            if (success)
                savingCalc.SetPeriod(period);
            else
            {
                MessageBox.Show("Invalid value for period");
                return;
            }
        }


        private void txtDeposit_TextChanged(object sender, EventArgs e)
        {
         ReadSavingInput();
        }

        private void txtPeriod_TextChanged(object sender, EventArgs e)
        {
         ReadPeriodInput();
        }


        private void txtFinBal_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtFinBal_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void lblAmPaid_Click(object sender, EventArgs e)
        {
           
        }

        private void lblBal_Click(object sender, EventArgs e)
        {

        }
    }
}
