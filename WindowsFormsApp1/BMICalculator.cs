﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class BMICalculator
    {
        private double height;
        private double weight;

        private UnitTypes unit;

        public BMICalculator()
        {
            unit = UnitTypes.Metric;
        }

        public double GetHeight()
        {
            return height;
        }
        public void SetHeight(double height)
        {
            if(height >= 0.0)
            this.height = height;
        }
        public double GetWeight()
        {
            return weight;
        }
        public void SetWeight(double weight)
        {
            if (weight >= 0)
                this.weight = weight;
        }
        public UnitTypes GetUnitType()
        {
            return unit;
        }
        public void SetUnitType(UnitTypes unit)
        {
            this.unit = unit;
        }
        public double CalculateBMI()
        {
            double bmiValue = 0.0;
            double factor = 1.0;

            if (unit == UnitTypes.Imperial)
            {
                factor = 703.0;
            }

            bmiValue = factor * weight / (height * height);

            return bmiValue;

        }
        public string BMIWeightCategory()
        {
            double bmi = CalculateBMI();
            string stringout = string.Empty;

            if (bmi < 18.5)
                stringout = "Underweight";

            else if (bmi <= 24.9)
                stringout = "Normal weight";

            else if (bmi <= 29.9)
                stringout = "Overweight (Pre-obesity)";

            else if (bmi <= 34.9)
                stringout = "Overweight (Obesity class I)";

            else if (bmi <= 39.9)
                stringout = "Overweight (Obesity class II)";

            else
                stringout = "Overweight (Obesity class III)";

            return stringout;
        }


    }
}
