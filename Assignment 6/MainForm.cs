﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment_6
{
    public partial class MainForm : Form
    {

        private TaskManager taskManager;

        string fileName = Application.StartupPath + "\\Tasks.txt";
        public MainForm()
        {
            InitializeComponent();
            InitializeGUI();
        }

        private void InitializeGUI()
        {
            this.Text = "ToDo Reminder by Simon Sandqvist";

            taskManager = new TaskManager();

            cmbPriority.Items.Clear();
            cmbPriority.Items.AddRange(Enum.GetNames(typeof(PriorityType)));
            cmbPriority.SelectedIndex = (int)PriorityType.Normal;

            lstTasks.Items.Clear();
            lblClock.Text = string.Empty;
            clockTimer.Start();

            txtDescription.Text = string.Empty;

            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "yyyy-MM-dd    HH:mm";

            menuFileOpen.Enabled = true;
            menuFileSave.Enabled = true;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem1_Click_1(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Task task = ReadInput();
            if (taskManager.AddNewTask(task))
            {
                UpdateGUI();
            }
        }

        private Task ReadInput()
        {
            Task task = new Task();

            if (string.IsNullOrEmpty(txtDescription.Text))
            {
                MessageBox.Show("No Subject? Write something please!", "Hello...");
                return null;
            }
            task.Description = txtDescription.Text;
            task.TaskDate = dateTimePicker1.Value;
            task.Priority = (PriorityType)cmbPriority.SelectedIndex;

            return task;
        }

        private void UpdateGUI()
        {
            lstTasks.Items.Clear();
            string[] infoStrings = taskManager.GetInfoStringsList();
            if (infoStrings != null)
                lstTasks.Items.AddRange(infoStrings);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void menuFileSave_Click(object sender, EventArgs e)
        {
            string errMessage = "Something went wrong while saving date to file";

            bool ok = taskManager.WriteDataToFile(fileName);
            if (!ok)
                MessageBox.Show(errMessage);
            else
                MessageBox.Show("Data saved to file:" + Environment.NewLine + fileName);
        }

        private void menuFileOpen_Click(object sender, EventArgs e)
        {
            string errMessage = "Something went wrong when opening the file";

            bool ok = taskManager.ReadDataFromFile(fileName);
            if (!ok)
            {

            
                MessageBox.Show(errMessage);
            }
            else
                UpdateGUI();
        }

        private void toolStripMenuItem3_Click_1(object sender, EventArgs e)
        {
            InitializeGUI();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void menuFileExit_Click(object sender, EventArgs e)
        {
            DialogResult dlgResult = MessageBox.Show("Sure to exit program?",
                "Think twice", MessageBoxButtons.OKCancel);
            if (dlgResult == DialogResult.OK)
                Application.Exit();
        }

        private void clockTimer_Tick(object sender, EventArgs e)
        {
            lblClock.Text = DateTime.Now.ToLongTimeString();
        }
    }

}
