﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_4
{
    class PhoneBook
    {
       
        private string[] names = { "Louise", "Jakob", "Thure", "Kyanh", "Ogge" };
        private string[] phones = { "072-100", "072-101", "072-102", "072-103", "072-104" };
        private string[,] phoneList;
        public PhoneBook()
        {
            Console.Clear();

            DisplayList();
            Console.WriteLine();

            Console.WriteLine("Sorted list:");

            SortByName();
            Console.WriteLine();

            DisplayList();
            Console.WriteLine();

            int count = names.Length;
            phoneList = new string[count, 2];
            FillTable();

            Console.WriteLine("\n\nUsing a 2nd  array instead  of  two 1d  arrays:\n");
            DisplayTable();
            
        }




        public void DisplayList()
        {
            for (int index = 0; index < 5; index++)
            {
                string strOut = string.Format("{0, -15} {1,-15}", names[index], phones[index]);
                Console.WriteLine(strOut);
            }

        }

        public void SortByName()
        {
            int pos, i;
            int length = names.Length;

            for (pos = 0; pos < length - 1; pos++)
            {
                for (i = 0; i < length - pos - 1; i++)
                {
                    int result = names[i].CompareTo(names[i + 1]);

                    if (result == 1)
                    {
                        SwapValues(i);
                    }
                }

            }
        }

        private void SwapValues(int pos)
        {
            string temp = names[pos];
            names[pos] = names[pos + 1];
            names[pos + 1] = temp;

            temp = phones[pos];
            phones[pos] = phones[pos + 1];
            phones[pos + 1] = temp;
        }
        private void FillTable()
        {
            for (int row = 0; row < names.Length; row++)
            {
                phoneList[row, 0] = names[row];
                phoneList[row, 1] = phones[row];
            }
        }
        private void DisplayTable()
        {
            int rows = phoneList.GetLength(0);
            int cols = phoneList.GetLength(1);

            for (int row = 0; row < rows; row++)
            {
                Console.Write(string.Format("{0,-8}", "Row" + row.ToString()));

                for (int col = 0; col < cols; col++)
                {
                    Console.Write(string.Format("{0,-15}", phoneList[row, col]));

                }
                Console.WriteLine();
            }

        }












    }
}
