﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_4_pt_2
{
    class PartyManager
    {

        private double costPerPerson;
        private double feePerPerson;
        private int numOfElems = 0;

        private string[] guestList;
        

        public PartyManager(int maxNumOfGuests)
        {
            guestList = new string[maxNumOfGuests];
            GetGuestList();
            
        }
        
        public double CostPerPerson
        {
            get { return costPerPerson; }
            set
            {
                if (value >= 0.0)
                    costPerPerson = value;
            }

        }

        public double FeePerPerson
        {
            
            get { return feePerPerson; }
            
            set
            {
                if (value >= 0.0)
                    feePerPerson = value;
            }

        }

        public int NumOfGuests()
        {
            int numGuests = 0;
            
            for (int i = 0; i < guestList.Length; i++)
            {
                if (!string.IsNullOrEmpty(guestList[i]))
                {
                    numGuests++;
                }
            }
            return numGuests;
        }
        public bool DeleteAt(int index)
        {
            bool ok = false;
            if (index < guestList.Length)
            {
                guestList[index] = string.Empty;
                guestList[index] = null;
                MoveElementsOneStepToLeft(index);
                ok = true;
            }
            return ok;

        }
        private int FindVacantPos()
        {
            int vacantPos = -1;
            for (int index = 0; index < guestList.Length; index++)
            {
                if (string.IsNullOrEmpty(guestList[index]))
                {
                    vacantPos= index;
                    break;

                }

            }
            return vacantPos;

        }
        public bool AddNewGuest(string firstName, string lastName)
        {
            bool ok = true;
            int vacantPos = FindVacantPos();

            if (vacantPos != -1)
            {
                guestList[vacantPos] = FullName(firstName, lastName);
            }
            else
                ok = false;

            return ok;
        }
        private string FullName(string firstName, string lastName)
        {
            return lastName.ToUpper() + ", " + firstName;

        }


        private void MoveElementsOneStepToLeft(int index)
        {
            for (int i = index + 1; i < guestList.Length; i++)
            {
                guestList[i - 1] = guestList[i];
                guestList[i] = string.Empty;

            }

        }
        public string[] GetGuestList()
        {
            int size = NumOfGuests();

            if (size <= 0)
                return null;

            string[] guests = new string[size];

            for (int i = 0, j = 0; i < guestList.Length; i++)
            {
                if (!string.IsNullOrEmpty(guestList[i]))
                {
                    guests[j++] = guestList[i];
                }

            }
            return guests; 

        }

        public double CalcTotalCost()
        {
            double totalCost = costPerPerson * NumOfGuests();
            return totalCost;
        }

        public double CalcTotalFee()
        {
            double totalFee = FeePerPerson * NumOfGuests();
            return totalFee;
        }

        public double CalcProfit()
        {
            double profit = CalcTotalFee() - CalcTotalCost();
            return profit;
        }

    }


}
