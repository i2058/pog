﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment_4_pt_2
{
    public partial class MainForm : Form
    {
        PartyManager partyManager;
        public MainForm()
        {
            InitializeComponent();
            InitializeGUI();
            
        }

        private void InitializeGUI()
        {
            ClearInputControls();
            ClearOutputControls();

            grpAddGuests.Enabled = false;
            grpNewParty.Enabled = true;

        }

        private void ClearInputControls()
        {
            txtMaxNum.Text = string.Empty;
            txtCost.Text = string.Empty;
            txtFee.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
        }

        private void ClearOutputControls()
        {
            lblNumGuests.Text = "0";
            lblTotalCost.Text = "0.00";
            lblTotalFees.Text = "0.00";
            lblProfit.Text = "0.00";
            lstAllGuests.Items.Clear();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)

        {
            partyManager.AddNewGuest(txtFirstName.Text, txtLastName.Text);
            UpdateGUI();


        }

        private void grpGuests_Enter(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void btnCreateList_Click(object sender, EventArgs e)
        {
            bool maxNumOK = CreateParty();
            if (!maxNumOK)
                return;

            bool amountOK = ReadCostPerPerson() && ReadFeePerPerson();

            if(maxNumOK && amountOK)
            {
                grpAddGuests.Enabled = true;
                UpdateGUI();
            }

        }
        private bool CreateParty()
        {
            int maxNumber = 0;
            bool ok = true;

            if (int.TryParse(txtMaxNum.Text, out maxNumber) && (maxNumber > 0))
            {
                partyManager = new PartyManager(maxNumber);
                MessageBox.Show($"Party list with space for {maxNumber} guests created!", "Success");
                
            }
            else
            {
                MessageBox.Show("Invalid total number. Please try again!", "Error");
                ok = false;
            }
            return ok;
        }
        private bool ReadCostPerPerson()
        {
            double lowestNumber = 0;
            bool ok = true;
           
            
            if (double.TryParse(txtCost.Text, out lowestNumber) && (lowestNumber > 0))
            {
                partyManager.CostPerPerson = lowestNumber;
                return ok;

            }
            else
            {
                MessageBox.Show("Invalid cost per person. Please try again!", "Error");

                return ok = false;
            }
            
        }
        private bool ReadFeePerPerson()
        {
            double lowestFee = 0;
            bool ok = true;

            if (double.TryParse(txtFee.Text, out lowestFee) && (lowestFee > 0))
            {
                partyManager.FeePerPerson = lowestFee;
                 return ok;

            }
            else
            {
                MessageBox.Show("Invalid fee per person. Please try again!", "Error");
                return ok = false;
            }
            

        }
        private void UpdateGUI()
        {
            lstAllGuests.Items.Clear();
            string[] guestList = partyManager.GetGuestList();

            if (guestList != null)
            {
                for (int i = 0; i < guestList.Length; i++)
                {
                    string str = $"{i + 1,4} {guestList[i],-20}";
                    lstAllGuests.Items.Add(str);
                }

            }
            else
                return;

            double totalCost = partyManager.CalcTotalCost();
            lblTotalCost.Text = totalCost.ToString( "0.00" );
            
            double totalFee = partyManager.CalcTotalFee();
            lblTotalFees.Text = totalFee.ToString("0.00");

            double profit = partyManager.CalcProfit();
            lblProfit.Text = profit.ToString("0.00");

            double numOfGuests = partyManager.NumOfGuests();
            lblNumGuests.Text = numOfGuests.ToString("0");

            
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
                int index = this.lstAllGuests.SelectedIndex;
                if (index == -1)
                {
                    return;
                }
                partyManager.DeleteAt(index);
                UpdateGUI(); 
        }
        private void lstAllGuests_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }




}
