﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{

    class Pet
    {
        // Declaration of variables: Pet-name, pet-age, and gender 
        private string name;
        private int age;
        private bool isFemale;

        public void Start()
        {
            //Opening line, welcome
            Console.WriteLine();
            Console.WriteLine("Greetings from the Pet Owner application!");
            Console.WriteLine();
            

            ReadAndSavePetData();
            DisplayPetInfo();
        }
        public void ReadAndSavePetData()
        {
            Console.Write("What is the name of your pet?");
            Console.WriteLine();
            name = Console.ReadLine();

            Console.Write("What is " + name + "'s age?");
            Console.WriteLine();
            string textValue = Console.ReadLine();
            age = int.Parse(textValue);

            Console.Write("Is your pet a female (y/n)? ");
            Console.WriteLine();
            string strGender = Console.ReadLine();
            strGender = strGender.Trim();
            char response = strGender[0];

            if ((response == 'y') || (response == 'Y'))
                isFemale = true;
            else
                isFemale = false;

        }
        public void DisplayPetInfo()
        {
            Console.WriteLine();

            string textOut = "Name: " + name + " Age: " + age;
            Console.WriteLine (textOut);

            Console.WriteLine();

            string textOut2 = "Name " + name +" is a good";

            if (isFemale == true)
                Console.WriteLine(textOut2 + " girl!");
            else
                Console.WriteLine(textOut2 + " boy!");
        }




        }


}