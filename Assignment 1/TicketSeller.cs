﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
   class TicketSeller
    {
        private string name;
        private double price = 100;
        private int numOfAdults;
        private int numOfChildren;

        private double amountToPay;
        public void Start()
        {
            Console.WriteLine();
            Console.WriteLine("Hello and welcome to the Kids' Fair!");
            Console.WriteLine("Here at the Kids' Fair, we offer a 75% discount for the children!");
            Console.WriteLine();

            ReadAndSaveTicData();
            DisplayTicInfo();
        }
        public void ReadAndSaveTicData()
        {
        Console.WriteLine("Please, state your name.");
        name = Console.ReadLine();
            Console.WriteLine();

            Console.WriteLine(name + ", how many adults are in your party?");
            string textValue = Console.ReadLine();
            numOfAdults = int.Parse(textValue);
            Console.WriteLine();

            Console.WriteLine("Lastly, how many children are in your party?");
            string textValue2 = Console.ReadLine();
            numOfChildren = int.Parse(textValue2);
            Console.WriteLine();

            string textOut = "Very well, " + name + ". Here is your receipt!";
            Console.WriteLine (textOut);
        }
        public void DisplayTicInfo()
        {
        amountToPay = (numOfChildren * price * 0.25) + (numOfAdults * price);
        Console.WriteLine();
        Console.WriteLine("Total amount: " + amountToPay + "$");
        Console.WriteLine("Thank you for your patience, " + name + ". Enjoy your time at the fair!");
        Console.WriteLine();
        }

    }
    
 

}
