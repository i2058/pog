﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2
{
    class SelectionAndIteration
    { 
    
        public SelectionAndIteration()  //Constructor with 3 methods
        {
            Start();

            ShowStringLength(); //Call this method to read a text and display amount of chars

            MakeMyDay(); //Call this method to read a num, 1 - 7 and display name of the day plus quote

            SumNumbers(); //Call this method to sum values between two integer numbers, as well as caluculate sum, then display

        }
        public void Start()
        {
            Console.WriteLine();
            Console.WriteLine("My name is: Simon Sandqvist and I am a student of the Spring 22 semester!"); //To avoid resubmission ;)
            Console.WriteLine();

        }
        public void ShowStringLength() //Creating method 1
        {
            string text = string.Empty;
            bool done = false;

            Console.WriteLine("Let me calculate the length of strings for you!");
            do
            {
                Console.WriteLine("Give me a text of any length, or press Enter to continue!\n");
                text = Console.ReadLine();

                int length = text.Length;

                if (length == 0) done = true; //If no text is typed in at this stage, task is taken as done and skipped

                else { //If text is typed in, convert it all to uppercase letters and list the number of chars in the string

                    string v = text.ToUpper();
                    Console.WriteLine(v);
                    Console.WriteLine("Number of chars: "+ length);

                        }
            } while (!done);
        }
        public void MakeMyDay() //Creating method 2
        {

            Console.WriteLine("I can predict your day for you.");
            Console.WriteLine();

            Console.WriteLine("Give a number between 1 and 7:");
            Console.WriteLine();

            int day = int.Parse(Console.ReadLine());
            string fortune = string.Empty;

            switch(day)
            { //List different outputs corresponding to the different days/numbers
                case 1:
                    fortune = "Monday: Keep Calm! You can fall apart!";
                    break;

                case 2: fortune = "Tuesday and Wednesday break your heart!";
                    break;

                case 3: fortune = "Thursday, Uuush, still one day to Friday!";
                    break;

                case 4: fortune = "It's Friday! You are in love!";
                    break;

                case 5: fortune = "Saturday, do nothing and do plenty of it!";
                    break;

                case 6: fortune = "And Sunday always comes too soon!";
                    break;

                case 7: fortune = "Not in a good mood? This is not a valid date!";
                    break;
            }
            Console.WriteLine();
            Console.WriteLine(fortune);
            Console.WriteLine();
            Console.WriteLine("Press Enter to continue!");
        }
        private int SumNumbers(int start, int end)
        {
            int sum = 0;
            for (int i = start; i <= end; i++)
                {
                sum = sum + i;

            }
            return sum;
        }

        public void SumNumbers() //Creating method 3 
        {
            Console.WriteLine();
            Console.Write("Give a start number: ");
            int startNum = int.Parse(Console.ReadLine());

            Console.WriteLine();
            Console.WriteLine("Give an end number:");
            int endNum = int.Parse(Console.ReadLine());

            int temp = startNum; //Saving "startNum" value in a temp int value

            if (startNum > endNum) //Making sure method works as intended even if start num > end num, guaranteeing a positive "sum"
            {
                startNum = endNum;
                endNum = temp;
            }
            int sum = SumNumbers(startNum, endNum);
            Console.WriteLine(sum);
        }
    
    
    
    
    
    
    
    
    
    }


   
}
